FROM leandatascience/jupyterlab-ds:latest
ENV MAIN_PATH=/usr/local/bin/covidatos
ENV LIBS_PATH=${MAIN_PATH}/libs
ENV CONFIG_PATH=${MAIN_PATH}/config
ENV NOTEBOOK_PATH=${MAIN_PATH}/notebooks

RUN pip3 install matplotlib

EXPOSE 8888

CMD cd ${MAIN_PATH} && sh config/run_jupyter.sh
