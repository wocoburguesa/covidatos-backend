#!/bin/bash

docker build -t covidatos -f config/covidatos.Dockerfile .
docker run -ti -v ${PWD}:/usr/local/bin/covidatos -p 8888:8888 covidatos