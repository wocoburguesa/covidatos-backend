
# Covidatos Backend

El notebook que procesa la información publicada por el Minsa en [su página de datos abiertos](https://www.datosabiertos.gob.pe/dataset/casos-positivos-por-covid-19-ministerio-de-salud-minsa) está en la carpeta `action-scripts`. El archivo `Covidatos.ipynb` es el notebook con todos los pasos (advertencia: es denso). Necesita que coloques en la misma carpeta el csv del Minsa con el nombre `cases_up_to_date.csv`.

El entorno de JupyterLab está basado en [JupyterLab-Configurator](https://www.lean-data-science.com). Su README está más abajo. Aquí su página para los interesados. Todo el crédito de la configuratión del JupyterLab es de ellos.

El sitio live de covidatos está [acá](https://covidatos.pe/), junto con más información de agradecimientos y uso.

Para cualquier duda pueden encontrarme en Twitter como @covidatos o en el mail covidatos.peru@gmail.com.

2020 - Marco Flores

## README de JupyterLab-Configurator

The [JupyterLab-Configurator](https://www.lean-data-science.com) lets you easily create your **JupyterLab configuration** that runs JupyterLab in a **container** and automates the whole setup using **scripts**. A container is a separated environment that encapsulates the libraries you install in it without affecting your host computer. Scripts automate executing all the commands you would normally need to run manually. For you can review and edit scripts, you get full control of your configuration at any time.

**Create** your JupyterLab configuration:

1. Create your configuration with a few clicks with the [**JupyterLab-Configurator**](https://www.lean-data-science.com)
1. Download and unzip your configuration
1. Customize it to your needs (optional)

The following picture shows the JupyterLab configuration in action. **Use** it with two simple steps:

1. Execute `sh {path_to_your_project}/run.sh`
1. Open `localhost:8888` from a browser

<table class="image">
<tr><td><img src="config_use.png" width="600"></td></tr>
<tr><td class="caption" >Using the JupyterLab configuration</td></tr>
</table>